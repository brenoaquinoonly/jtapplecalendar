//
//  CustomCell.swift
//  JTAppleTeste
//
//  Created by Breno Aquino on 20/09/2017.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit
import JTAppleCalendar

class CustomCell: JTAppleCell {
    
    @IBOutlet weak var dateLabe: UILabel!
    
    @IBOutlet weak var viewBackground: UIView!
    
    var indiceBorder = -1
}

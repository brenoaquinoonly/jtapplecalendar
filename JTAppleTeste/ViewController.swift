//
//  ViewController.swift
//  JTAppleTeste
//
//  Created by Breno Aquino on 20/09/2017.
//  Copyright © 2017 Breno Aquino. All rights reserved.
//

import UIKit
import JTAppleCalendar

extension CALayer {
    
    func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) -> Int {
        
        let border = CALayer()
        
        switch edge {
        case UIRectEdge.top:
            border.frame = CGRect.init(x: 0, y: 0, width: frame.width, height: thickness)
            break
        case UIRectEdge.bottom:
            border.frame = CGRect.init(x: 0, y: frame.height - thickness, width: frame.width, height: thickness)
            break
        case UIRectEdge.left:
            border.frame = CGRect.init(x: 0, y: 0, width: thickness, height: frame.height)
            break
        case UIRectEdge.right:
            border.frame = CGRect.init(x: frame.width - thickness, y: 0, width: thickness, height: frame.height)
            break
        default:
            break
        }
        
        border.backgroundColor = color.cgColor;
        
        self.addSublayer(border)
        
        if let sublayers = self.sublayers {
            
            return (sublayers.count - 1)
        }
        
        return -1
    }
}

class ViewController: UIViewController {

    @IBOutlet weak var calendarView: JTAppleCalendarView!
    
    let formatter = DateFormatter()
    
    var startDate = Date()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.calendarView.minimumInteritemSpacing = 0
    }
    
    func choseThisMonth() {
        
        formatter.dateFormat = "MM"
        
        let indexPathForFirstRow = IndexPath(row: 0, section: Int(formatter.string(from: Date()))! - 1)
        
        self.calendarView.selectItem(at: indexPathForFirstRow, animated: true, scrollPosition: .left)
    }
}

extension ViewController: JTAppleCalendarViewDataSource {
    
    func configureCalendar(_ calendar: JTAppleCalendarView) -> ConfigurationParameters {
        
        formatter.dateFormat = "dd/MM/yyyy"
        
        formatter.timeZone = Calendar.current.timeZone
        
        formatter.locale = Calendar.current.locale
        
        let endDate = formatter.date(from: "31/12/2017")!
        
        startDate = formatter.date(from: "01/01/2017")!
        
        let parametrers = ConfigurationParameters(startDate: startDate, endDate: endDate)
        
        return parametrers
    }
}

extension ViewController: JTAppleCalendarViewDelegate {
    
    func calendar(_ calendar: JTAppleCalendarView, cellForItemAt date: Date, cellState: CellState, indexPath: IndexPath) -> JTAppleCell {
        
        let cell = calendar.dequeueReusableJTAppleCell(withReuseIdentifier: "CustomCell", for: indexPath) as! CustomCell
        
        cell.dateLabe.text = cellState.text
        
        if cellState.dateBelongsTo == .thisMonth {
            
            if cell.indiceBorder == -1 {
                
                cell.indiceBorder = cell.layer.addBorder(edge: .top, color: .black, thickness: 1)
            }
            
            cell.dateLabe.isHidden = false
            
            cell.dateLabe.textColor = .black
            
        } else {

            if cell.indiceBorder != -1 {
                
                cell.layer.sublayers?.remove(at: cell.indiceBorder)
                
                cell.indiceBorder = -1
            }
            
            cell.dateLabe.isHidden = true
        }
        
        cell.viewBackground.isHidden = true
        
        cell.viewBackground.layer.cornerRadius = cell.viewBackground.frame.width / 2
        
        print("Section: \(indexPath.section) | Row: \(indexPath.row)")
        
        formatter.dateFormat = "dd/MM/yyyy"
        
        let endDate = formatter.date(from: "01/01/2017")!
        
        if date == endDate {
            
            print("AQUI")
            
            self.choseThisMonth()
        }
        
        return cell
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        if let cellCustom = cell as? CustomCell {
            
            cellCustom.viewBackground.isHidden = false
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, didDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) {
        
        if let cellCustom = cell as? CustomCell {
            
            cellCustom.viewBackground.isHidden = true
        }
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldSelectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        
        return false
    }
    
    func calendar(_ calendar: JTAppleCalendarView, shouldDeselectDate date: Date, cell: JTAppleCell?, cellState: CellState) -> Bool {
        
        return false
    }
}
